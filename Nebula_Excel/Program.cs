﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Nebula_Excel
{
    class Program
    {
        private static string workingDir = "C:\\Users\\lradmin\\Documents\\TD\\Nebula";

        static void Main(string[] args)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            Execute(args);
            //DebugMethod(args);

            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);

            Console.WriteLine(Environment.NewLine + "Press any key to close...");
            Console.ReadKey();
        }

        static void Execute(string[] args)
        {
            string[] filePaths;

            if (args.Length > 0 && File.Exists(args[0]))
            {
                filePaths = new string[] { args[0] };
            }
            else
            {
                //filePaths = Directory.GetFiles(Path.Combine(Directory.GetCurrentDirectory(),"\\Upload"), "*.csv", SearchOption.TopDirectoryOnly);
                filePaths = Directory
                    .EnumerateFiles(workingDir + "\\Upload")
                    .Where(f => f.ToLower().EndsWith("csv") || f.ToLower().EndsWith("mdb"))
                    .ToArray();
            }

            Console.WriteLine("Files to process: " + filePaths.Length);

            foreach (string uploadPath in filePaths)
            {
                StartProcessing(uploadPath);

                switch (Path.GetExtension(uploadPath).ToLower())
                {
                    case ".csv":
                        ProcessJMeterCSV(uploadPath);
                        break;
                    case ".mdb":
                        ProcessAccessDB(uploadPath);
                        break;
                    default:
                        Console.WriteLine("ERROR - File not processed: {0}", uploadPath);
                        break;
                }

                EndProcessing(uploadPath);
            }
        }

        static void ProcessJMeterCSV(string uploadPath)
        {
            string outputPath = workingDir + "\\Output\\" + Path.GetFileNameWithoutExtension(uploadPath) + ".xlsx";
            ExcelChartBuilder test = new ExcelChartBuilder();
            JMeterCSVParser csvParser = new JMeterCSVParser();

            csvParser.Parse(uploadPath);
            test.CreateExcel(outputPath, csvParser.GetResultsTotalArray(), csvParser.GetResponseByTransactionArray(), csvParser.GetResponseByThreadsArray());
        }

        static void ProcessAccessDB(string uploadPath)
        {
            string outputPath = workingDir + "\\Output\\" + Path.GetFileNameWithoutExtension(uploadPath) + ".xlsx";
            ExcelChartBuilder test = new ExcelChartBuilder();
            AccessDB db = new AccessDB(uploadPath);

            db.Exec();
            test.CreateExcel(outputPath, db.GetTotalArray(), db.GetResponseByTransaction(), db.GetResponseByThread());
        }

        static void StartProcessing(string uploadPath)
        {
            Console.WriteLine("Started processing " + Path.GetFileName(uploadPath));
        }

        static void EndProcessing(string uploadPath)
        {
            string ext = Path.GetExtension(uploadPath);
            string archivePath = workingDir + "\\Archive\\" + Path.GetFileNameWithoutExtension(uploadPath);

            //Checks if file name already exists in the archive, and increments number accordingly
            if (File.Exists(archivePath + ext))
            {
                int fileCount = 1;
                while (File.Exists(archivePath + " (" + fileCount + ")" + ext))
                {
                    fileCount++;
                }

                archivePath += " (" + fileCount + ")";
            }

            //Moves processed input file to the Archive
            File.Move(uploadPath, archivePath + ext);

            Console.WriteLine("Finished processing " + Path.GetFileName(uploadPath));
        }

        static void DebugMethod(String[] args)
        {
            //String uploadPath = "C:\\Users\\lradmin\\Documents\\Visual Studio 2017\\Projects\\Nebula_Excel\\Upload\\Test2.csv";
            //String uploadPath = "Z:\\Overall Performance\\Thomas Dann\\Source\\Nebula_Excel\\Upload\\1989850007ATWS.mdb";
            //String uploadPath = "\\\\10.10.10.2\\c$\\Users\\lradmin\\AppData\\Local\\Temp\\1989850007ATWS.mdb";
            //String outputPath = "C:\\Users\\lradmin\\Documents\\Visual Studio 2017\\Projects\\Nebula_Excel\\Output\\Test2.xlsx";

            var directory = new DirectoryInfo("\\\\10.10.10.2\\c$\\Users\\lradmin\\AppData\\Local\\Temp\\");
            var dbFile = (from f in directory.GetFiles("*.mdb")
                          orderby f.LastWriteTime descending
                          select f).First();

            String outputPath = "C:\\Users\\lradmin\\Documents\\Visual Studio 2017\\Projects\\Nebula_Excel\\Output\\" + Path.GetFileNameWithoutExtension(dbFile.Name) + ".xlsx";

            ExcelChartBuilder test = new ExcelChartBuilder();
            //test.CreateJMeterCharts(uploadPath, outputPath);
            //test.CreateLRCharts(uploadPath,outputPath);
            AccessDB db = new AccessDB(dbFile.FullName);
            db.Exec();
            test.CreateExcel(outputPath, db.GetTotalArray(), db.GetResponseByTransaction(), db.GetResponseByThread());
        }
    }
}