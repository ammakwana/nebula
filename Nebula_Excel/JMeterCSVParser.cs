﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Nebula_Excel
{
    class JMeterCSVParser
    {
        //Variable setup
        private static String success, label, threadName;
        private static long elapsed, latency;
        private static int allThreads, maxPeriod;
        private static long timeStamp, firstTimeStamp, lastTimeStamp;
        private static ConcurrentDictionary<long, double> tpsMap;
        private static ConcurrentDictionary<long, double> respMap;
        private static ConcurrentDictionary<long, double> threadsMap;
        private static ConcurrentDictionary<long, double> latencyMap;
        private static ConcurrentDictionary<long, double> errorRateMap;
        private static ConcurrentDictionary<long, int> errorMap;
        private static ConcurrentDictionary<long, int> successMap;
        private static Dictionary<String, int> columnIndex;

        //Variables for recording response time by transaction
        private static List<string> transactionNames;
        private static Dictionary<long, Dictionary<string, Tuple<double, int>>> respByTransaction;

        //Variables for recording response time by thread
        private static List<string> threadNames;
        private static Dictionary<long, Dictionary<string, Tuple<double, int>>> respByThread;

        private static long timePeriod = 0;
        private static DateTime epochDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        private static Object missingVal = "";

        // Number of seconds to group by (e.g. 60 = Group values by every minute)
        private static int timeGroup = 60;

        //Output arrays
        Object[,] respTotalArr;
        Object[,] respByTransactionArr;
        Object[,] respByThreadsArr;

        public void MapCreator()
        {
            //Initialises response by transaction variable Dictionary<'timestamp', Dictionary<'transactionName', Tuple<'responseTime', 'successCount'>>>
            respByTransaction = new Dictionary<long, Dictionary<string, Tuple<double, int>>>();

            //Initialises response by thread variable Dictionary<'timestamp', Dictionary<'threadName', Tuple<'responseTime', 'successCount'>>>
            respByThread = new Dictionary<long, Dictionary<string, Tuple<double, int>>>(); tpsMap = new ConcurrentDictionary<long, double>();

            respMap = new ConcurrentDictionary<long, double>();
            threadsMap = new ConcurrentDictionary<long, double>();
            latencyMap = new ConcurrentDictionary<long, double>();
            errorRateMap = new ConcurrentDictionary<long, double>();
            errorMap = new ConcurrentDictionary<long, int>();
            successMap = new ConcurrentDictionary<long, int>();
            transactionNames = new List<string>();
            threadNames = new List<string>();
            columnIndex = new Dictionary<String, int>()
            {
                {"timeStamp", 0},
                {"elapsed", 1},
                {"label", 2},
                {"responseCode", 3},
                {"responseMessage", 4},
                {"threadName", 5},
                {"dataType", 6},
                {"success", 7},
                {"bytes", 8},
                {"grpThreads", 9},
                {"allThreads", 10},
                {"Latency", 11},
                {"Connect", 12}
            };
            timePeriod = 0;
        }

        //Processes JMeter CSV File
        public void Parse(String filePath)
        {
            Regex threadRegex = new Regex("\\s\\d+\\-\\d+$");
            MapCreator();

            Console.WriteLine("Started parsing JMeter CSV file...");

            using (StreamReader reader = new StreamReader(File.OpenRead(filePath)))
            {
                var data = CsvParser.ParseHeadAndTail(reader, ',', '"');
                var header = data.Item1;
                IEnumerable<IList<string>> records = data.Item2;

                GetColumnNumbers(header);
                
                String timestampValue, labelValue, threadNameValue;

                firstTimeStamp = long.MaxValue;
                lastTimeStamp = long.MinValue;

                foreach (var values in records)
                {
                    timestampValue = values[columnIndex["timeStamp"]];

                    //Compares to first timestamp value
                    if (TimeStampToMilliseconds(timestampValue) < firstTimeStamp)
                    {
                        firstTimeStamp = TimeStampToMilliseconds(timestampValue);
                    }

                    //Compares to last timestamp value
                    if (TimeStampToMilliseconds(timestampValue) > lastTimeStamp)
                    {
                        lastTimeStamp = TimeStampToMilliseconds(timestampValue);
                    }

                    //Adds label to list of transaction names
                    labelValue = values[columnIndex["label"]];
                    if (!transactionNames.Contains(labelValue))
                    {
                        transactionNames.Add(labelValue);
                    }

                    //Regex to remove trailing script/thread number
                    threadNameValue = threadRegex.Replace(values[columnIndex["threadName"]], "");
                    if (!threadNames.Contains(threadNameValue))
                    {
                        threadNames.Add(threadNameValue);
                    }

                    //Finds values from row
                    timeStamp = TimeStampToMilliseconds(values[columnIndex["timeStamp"]]);
                    elapsed = long.Parse(values[columnIndex["elapsed"]]);
                    success = values[columnIndex["success"]].ToLower();
                    allThreads = int.Parse(values[columnIndex["allThreads"]]);
                    latency = long.Parse(values[columnIndex["Latency"]]);
                    label = values[columnIndex["label"]];
                    threadName = threadRegex.Replace(values[columnIndex["threadName"]], "");
                    
                    MapRecord();
                }

                maxPeriod = (int)((lastTimeStamp - firstTimeStamp) / (1000 * timeGroup)) + 1;

                transactionNames.Sort();
                threadNames.Sort();
            }

            CalculateAverages();

            CreateResultsArrays();

            Console.WriteLine("Finished parsing JMeter CSV file...");

            // String outputPath = "C:\\Users\\thdann\\Documents\\Nebula\\Output\\";
            // outputResultsTotal(outputPath);
            // outputResultsByTransaction(outputPath);

        }

        private static void GetColumnNumbers(IList<string> header)
        {
            String columnName;

            for (int i = 0; i < header.Count; i++)
            {
                columnName = header[i];
                if (columnIndex.ContainsKey(columnName))
                {
                    columnIndex[columnName] = i;
                }
            }
        }

        private static long TimeStampToMilliseconds(String timeStamp)
        {
            // JMeter rawCSV files use timeStamp in date format
            long time = 0L;

            try
            {
                //Converts timestamp to format of milliseconds since Epoch
                DateTime date = DateTime.Parse(timeStamp);
                time = (long)date.Subtract(epochDate).TotalMilliseconds;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + e.StackTrace);
            }
            return time;
        }

        private static String TimePeriodToString(int period)
        {
            return TimePeriodToDateTime(period).ToShortTimeString();
        }

        private static DateTime TimePeriodToDateTime(int period)
        {
            DateTime date = epochDate;
            long milliseconds = firstTimeStamp + (period * timeGroup * 1000);
            return date.AddMilliseconds(milliseconds);
        }

        private static long MillisecondsToTimePeriod(long ms)
        {
            return (long)(ms / (timeGroup * 1000));
        }

        private static long ShortToLongTimePeriod(int i)
        {
            return i + MillisecondsToTimePeriod(firstTimeStamp);
        }

        private static int LongToShortTimePeriod(long i)
        {
            return (int)(i - MillisecondsToTimePeriod(firstTimeStamp));
        }
        
        private void MapRecord()
        {
            //int index = 0;
            timePeriod = MillisecondsToTimePeriod(timeStamp);

            if (success.Equals("true"))
            {
                //Increments success count
                successMap.AddOrUpdate(timePeriod, 1, (key, count) => count + 1);

                //Adds to response time total
                respMap.AddOrUpdate(timePeriod, elapsed, (key, total) => total + elapsed);

                //Adds to latency total
                latencyMap.AddOrUpdate(timePeriod, latency, (key, total) => total + latency);

                //Adds to thread total
                threadsMap.AddOrUpdate(timePeriod, allThreads, (key, total) => total + allThreads);

                //Adds to reponse time per transaction
                UpdateDictionaryWithStats(ref respByTransaction, timePeriod, label);

                //Adds to reponse time per thread
                UpdateDictionaryWithStats(ref respByThread, timePeriod, threadName);
                
            }
            else if (success.Equals("false"))
            {
                //Increments error count
                errorMap.AddOrUpdate(timePeriod, 1, (key, count) => count + 1);
            }
        }

        private void UpdateDictionaryWithStats(ref Dictionary<long, Dictionary<string, Tuple<double, int>>> dict, long key1, string key2)
        {
            Dictionary<string, Tuple<double, int>> tempDict;
            Tuple<double, int> statTuple;

            if (dict.ContainsKey(key1))
            {
                tempDict = dict[key1];

                if (tempDict.ContainsKey(key2))
                {
                    //Data already recorded for timestamp and transaction/thread name, so increment current value
                    statTuple = tempDict[key2];
                    statTuple = Tuple.Create(statTuple.Item1 + elapsed, statTuple.Item2 + 1);
                    tempDict[key2] = statTuple;
                }
                else
                {
                    //Data recorded for timestamp but not transaction.thread name, so create new data tuple
                    statTuple = new Tuple<double, int>(elapsed, 1);
                    tempDict.Add(key2, statTuple);
                }

                dict[key1] = tempDict;
            }
            else
            {
                //Data not recorded for timestamp so create new transaction/thread dictionary for that timestamp
                statTuple = new Tuple<double, int>(elapsed, 1);
                tempDict = new Dictionary<string, Tuple<double, int>>() { { key2, statTuple } };
                dict.Add(key1, tempDict);
            }
        }

        private void CalculateAveragesForDictionary(ref Dictionary<long, Dictionary<string, Tuple<double, int>>> dict)
        {
            Tuple<double, int> statTuple;
            foreach (long key1 in dict.Keys.ToList())
            {
                foreach (string key2 in dict[key1].Keys.ToList())
                {
                    statTuple = dict[key1][key2];
                    dict[key1][key2] = Tuple.Create(statTuple.Item1 / statTuple.Item2, statTuple.Item2);
                }
            }
        }

        private void CalculateAverages()
        {
            bool valUpdated;

            //Loops over all timeperiod with successful transactions
            foreach (var period in successMap.Keys)
            {
                if (successMap.TryGetValue(period, out int successCount))
                {
                    //Calculates and adds transactions per second
                    valUpdated = tpsMap.TryAdd(period, (double)successCount / (double)timeGroup);

                    //Calculates average response time per successful transaction
                    respMap[period] = (double)respMap[period] / (double)successCount;

                    //Calculates threads during time period
                    threadsMap[period] = (double)threadsMap[period] / (double)successCount;

                    //Calculates average latency per successful transaction
                    latencyMap[period] = (double)latencyMap[period] / (double)successCount;

                    //Calculates error rate
                    if (errorMap.TryGetValue(period, out int errorCount))
                    {
                        valUpdated = errorRateMap.TryAdd(period, (double)errorCount / (double)(successCount + errorCount));
                    }

                }
                else
                {
                    throw new KeyNotFoundException("Key not found");
                }
            }

            //Finds average of response time by transaction
            CalculateAveragesForDictionary(ref respByTransaction);

            //Finds average of response time by thread
            CalculateAveragesForDictionary(ref respByThread);
        }

        public void SetTimePeriod(int seconds)
        {
            if (seconds > 0)
            {
                timeGroup = seconds;
            }
            else
            {
                throw new InvalidDataException("Time period must be larger than 0");
            }
        }

        private String ArrayToCSVString(Object[,] arr)
        {
            //Method to convert a Object[,] array to a string in CSV format
            String str = "";

            for (int i = 0; i < arr.GetLength(0); i++)
            {
                //Adds first value without comma
                str += arr[i, 0];
                //Adds rest of row data including columns
                for (int j = 1; j < arr.GetLength(1); j++)
                {
                    str += "," + arr[i, j].ToString();
                }

                str += Environment.NewLine;
            }

            return str;
        }

        public void OutputResultsTotal(String outputPath)
        {
            String csv = ArrayToCSVString(GetResultsTotalArray());
            System.IO.File.WriteAllText(outputPath + "Total.CSV", csv);
        }

        public void OutputResultsByTransaction(String outputPath)
        {
            String csv = ArrayToCSVString(GetResponseByTransactionArray());
            System.IO.File.WriteAllText(outputPath + "Response_Times_by_Transaction.CSV", csv);
        }

        public void OutputResultsByThread(String outputPath)
        {
            String csv = ArrayToCSVString(GetResponseByThreadsArray());
            System.IO.File.WriteAllText(outputPath + "Response_Times_by_Thread.CSV", csv);
        }

        /*
        GETTER METHODS
        */

        public int GetTimePeriod()
        {
            return timeGroup;
        }

        public int GetMaxPeriod()
        {
            return maxPeriod;
        }

        #region OldGetterMethods
        /* public double[,] GetResponseByThread()
        {
            return respByThread;
        }
        */

        /* public double[] GetTPS()
        {
            double[] output = new double[maxPeriod];

            for (int i = 0; i < maxPeriod; i++)
            {
                if (tpsMap.TryGetValue(i, out double doubleValue))
                {
                    output[i] = doubleValue;
                }
                else
                {
                    output[i] = 0;
                }
            }

            return output;
        }
        */

        /* public double[] GetResponseTimes()
        {
            double[] output = new double[maxPeriod];

            for (int i = 0; i < maxPeriod; i++)
            {
                if (respMap.TryGetValue(i, out double doubleValue))
                {
                    output[i] = doubleValue;
                }
                else
                {
                    output[i] = 0;
                }
            }

            return output;
        }
        */

        /*public double[] GetThreads()
        {
            double[] output = new double[maxPeriod];

            for (int i = 0; i < maxPeriod; i++)
            {
                if (threadsMap.TryGetValue(i, out double doubleValue))
                {
                    output[i] = doubleValue;
                }
                else
                {
                    output[i] = 0;
                }
            }

            return output;
        }
        */

        /* public double[] GetLatency()
        {
            double[] output = new double[maxPeriod];

            for (int i = 0; i < maxPeriod; i++)
            {
                if (latencyMap.TryGetValue(i, out double doubleValue))
                {
                    output[i] = doubleValue;
                }
                else
                {
                    output[i] = 0;
                }
            }

            return output;
        }
        */

        /* public double[] GetErrorRate()
        {
            double[] output = new double[maxPeriod];

            for (int i = 0; i < maxPeriod; i++)
            {
                if (errorRateMap.TryGetValue(i, out double doubleValue))
                {
                    output[i] = doubleValue;
                }
                else
                {
                    output[i] = 0;
                }
            }

            return output;
        }
        */

        /* public int[] GetSuccessCount()
        {
            int[] output = new int[maxPeriod];

            for (int i = 0; i < maxPeriod; i++)
            {
                if (successMap.TryGetValue(i, out int intValue))
                {
                    output[i] = intValue;
                }
                else
                {
                    output[i] = 0;
                }
            }

            return output;
        }
        */

        /* public int[] GetErrorCount()
        {
            int[] output = new int[maxPeriod];

            for (int i = 0; i < maxPeriod; i++)
            {
                if (successMap.TryGetValue(i, out int intValue))
                {
                    output[i] = intValue;
                }
                else
                {
                    output[i] = 0;
                }
            }

            return output;
        }
        */

        /* public DataTable GetResponseByTransaction()
        {
            DataTable dt = new DataTable();
            DataRow row;
            double val;

            dt.Columns.Add("Time", typeof(string));

            foreach (String columnName in transactionNames)
            {
                dt.Columns.Add(columnName, typeof(double));
            }

            for (int i = 0; i < respByTransaction.GetLength(0); i++)
            {
                row = dt.NewRow();
                row["Time"] = TimePeriodToString(i);

                for (int j = 0; j < respByTransaction.GetLength(1); j++)
                {
                    val = respByTransaction[i, j];
                    if (val.Equals(double.NaN))
                    {
                        row[transactionNames[j]] = 0;
                    }
                    else
                    {
                        row[transactionNames[j]] = respByTransaction[i, j];
                    }
                }

                dt.Rows.Add(row);
            }

            return dt;
        }
        */
        #endregion

        private void CreateResultsArrays()
        {
            respTotalArr = CreateResultsTotalArray();
            respByTransactionArr = CreateResponseByTransactionArray();
            respByThreadsArr = CreateResponseByThreadsArray();
        }

        private Object[,] CreateResultsTotalArray()
        {
            List<string> headerNames = new List<string>(new string[]
                {
                    "Time",
                    "TPS",
                    "Response Time",
                    "Threads",
                    "Latency",
                    "Success",
                    "Error",
                    "Error Rate (%)"
                });
            double[,] data = new double[maxPeriod, headerNames.Count];
            Object[,] arr = new object[maxPeriod + 1, headerNames.Count + 1];

            //Adds column headers to output
            for (int j = 0; j < headerNames.Count; j++)
            {
                arr[0, j] = headerNames[j];
            }

            //Adds data values to output
            for (int i = 0; i < maxPeriod; i++)
            {
                int j = 0;
                long timeKey = ShortToLongTimePeriod(i);

                //Time
                arr[i + 1, 0] = TimePeriodToString(i);
                j++;
                
                //TPS
                if (tpsMap.TryGetValue(timeKey, out double doubleValue))
                {
                    arr[i + 1, j] = doubleValue;
                }
                else
                {
                    arr[i + 1, j] = 0;
                }
                j++;
                
                //Response Time
                if (respMap.TryGetValue(timeKey, out doubleValue))
                {
                    arr[i + 1, j] = doubleValue;
                }
                else
                {
                    arr[i + 1, j] = missingVal;
                }
                j++;

                //Threads
                if (threadsMap.TryGetValue(timeKey, out doubleValue))
                {
                    arr[i + 1, j] = doubleValue;
                }
                else
                {
                    arr[i + 1, j] = 0;
                }
                j++;

                //Latency
                if (latencyMap.TryGetValue(timeKey, out doubleValue))
                {
                    arr[i + 1, j] = doubleValue;
                }
                else
                {
                    arr[i + 1, j] = 0;
                }
                j++;

                //Success
                if (successMap.TryGetValue(timeKey, out int intValue))
                {
                    arr[i + 1, j] = intValue;
                }
                else
                {
                    arr[i + 1, j] = 0;
                }
                j++;

                //Error
                if (errorMap.TryGetValue(timeKey, out intValue))
                {
                    arr[i + 1, j] = intValue;
                }
                else
                {
                    arr[i + 1, j] = 0;
                }
                j++;

                //Error Rate
                if (errorRateMap.TryGetValue(timeKey, out doubleValue))
                {
                    arr[i + 1, j] = doubleValue;
                }
                else
                {
                    arr[i + 1, j] = 0;
                }
                j++;
            }

            return arr;
        }

        private Object[,] CreateResponseByTransactionArray()
        {
            return DictToArrayWithTimePeriods(transactionNames, respByTransaction);
        }

        private Object[,] CreateResponseByThreadsArray()
        {
            return DictToArrayWithTimePeriods(threadNames, respByThread);
        }

        public Object[,] GetResultsTotalArray()
        {
            return respTotalArr;
        }

        public Object[,] GetResponseByTransactionArray()
        {
            return respByTransactionArr;
        }

        public Object[,] GetResponseByThreadsArray()
        {
            return respByThreadsArr;
        }

        /*private Object[,] AddTimePeriodsToArray(List<string> headerNames, double[,] data)
        {
            Object[,] arr = new Object[data.GetLength(0) + 1, data.GetLength(1) + 1];
            double val;

            if (data.GetLength(1) != headerNames.Count)
            {
                throw new Exception("Arrays must have the same width");
            }

            //Adds column headers to output
            arr[0, 0] = "Time";

            for (int j = 0; j < headerNames.Count; j++)
            {
                arr[0, j + 1] = headerNames[j];
            }

            //Adds data values to output
            for (int i = 0; i < data.GetLength(0); i++)
            {
                //Time value is added to the first column
                arr[i + 1, 0] = TimePeriodToString(i);

                //Data array is input into relevant row/column of ouput
                for (int j = 0; j < data.GetLength(1); j++)
                {
                    val = data[i, j];

                    if (val.Equals(double.NaN))
                    {
                        arr[i + 1, j + 1] = 0;
                    }
                    else
                    {
                        arr[i + 1, j + 1] = val;
                    }
                }
            }

            return arr;
        }
        */

        private Object[,] DictToArrayWithTimePeriods(List<string> headerNames, Dictionary<long, Dictionary<string, Tuple<double, int>>> dict)
        {
            Object[,] arr = new Object[maxPeriod + 1, headerNames.Count + 1];
            Object val;

            //Adds column headers to output
            arr[0, 0] = "Time";

            for (int j = 0; j < headerNames.Count; j++)
            {
                arr[0, j + 1] = headerNames[j];
            }

            //Adds data values to output
            for (int i = 0; i < maxPeriod; i++)
            {
                //Time value is added to the first column
                arr[i + 1, 0] = TimePeriodToString(i);

                //Data array is input into relevant row/column of ouput
                for (int j = 0; j < headerNames.Count; j++)
                {
                    val = missingVal;
                    bool exists = dict.TryGetValue(ShortToLongTimePeriod(i), out Dictionary<string, Tuple<double, int>> tempDict);
                    if (exists)
                    {
                        exists = tempDict.TryGetValue(headerNames[j], out Tuple<double, int> statTuple);
                        if (exists)
                        {
                            val = statTuple.Item1;
                        }
                    }
                    
                    arr[i + 1, j + 1] = val;
                }
            }

            return arr;
        }
    }
}