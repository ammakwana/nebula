﻿using System;
using Analysis.Api;
using Analysis.ApiLib;
using Analysis.ApiSL;

namespace Nebula_Excel
{
    class LR_API
    {
        public LR_API()
        {

        }

        public void CreateAnalysisApiObject()
        {
            LrAnalysis analysisApi;
            analysisApi = new LrAnalysis();
            Session currentSession = null;
            currentSession = analysisApi.Session;
        }

        /*
    Create session
    The following code shows how to create session from an existing LRR file.
    the new session is automatically stored in LRA file and opened.
    */
        public bool CreateSession(string sessionname, string resname)
        {
            //create new session from specified .lrr file and store it into
            //specified .lra file
            //You must setup RunTimeErrors object to "Silent mode"
            LrAnalysis analysisApi;
            analysisApi = new LrAnalysis();
            Session currentSession = null;
            currentSession = analysisApi.Session;

            bool result = analysisApi.Session.Create(sessionname, resname);

            try
            {
                if (result)
                {
                    //work with created session
                    //created session opened automatically
                }
                else
                {
                    Console.WriteLine(analysisApi.RunTimeErrors.LastErrorMessage);
                }
            }
            finally
            {
                analysisApi.Session.Close();
            }

            return result;
        }

    }
}