﻿using System;
using Excel = Microsoft.Office.Interop.Excel;
using ChartElement = Microsoft.Office.Core.MsoChartElementType;
using System.Collections.Generic;

namespace Nebula_Excel
{
    class ExcelChartBuilder
    {
        private static int maxPeriod;
        private static int msThreshold = 3000;
        private static string userCol, responseTimeCol, errorRateCol;

        public void CreateExcel(String outputPath, Object[,] totalArr, Object[,] respByTransaction, Object[,] respByThreads)
        {
            maxPeriod = totalArr.GetLength(0) - 1;

            Console.Write("Starting Excel File Creation...");
            
            Excel.Application excel = new Excel.Application();
            Excel.Workbook workbook = excel.Workbooks.Add(Type.Missing);
            Excel.Worksheet sheet = (Excel.Worksheet)workbook.ActiveSheet;
            Excel.Range chartRange;
            Excel.Chart chart;

            excel.Visible = false;
            excel.DisplayAlerts = false;

            #region Total Results Sheet
            //Total results
            sheet.Name = "Total Results";
            chartRange = ArrayToSheet(sheet, totalArr);

            //Finds column name
            Excel.Range headerRow = chartRange.Rows[1].Cells;
            userCol = FindColumnName(headerRow, "Threads");
            responseTimeCol = FindColumnName(headerRow, "Response Time");
            errorRateCol = FindColumnName(headerRow, "Error Rate (%)");

            //Formats error rate as percentage
            sheet.Range[errorRateCol + "2:" + errorRateCol + (maxPeriod + 1)].NumberFormat = "0.00%";

            //Adds overall response time graph to total results
            chartRange = sheet.Range["A1:A" + (maxPeriod + 1) + "," + responseTimeCol + "1:" + responseTimeCol + (maxPeriod + 1)];
            chart = CreateChart(sheet, chartRange);
            chart.ChartTitle.Text = "Overall Repsonse Time";
            AddSecondaryOverlay(chart, errorRateCol, "Error Rate", "Error Rate", (int)Excel.XlRgbColor.rgbRed);
            chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlSecondary).MaximumScale = 1;
            #endregion

            #region Response By Transaction Sheet
            //Response by transaction
            sheet = workbook.Worksheets.Add(After: workbook.Sheets[workbook.Sheets.Count]);
            sheet.Name = "Response by Transaction";
            chartRange = ArrayToSheet(sheet, respByTransaction);
            chart = CreateChart(sheet, chartRange, true, DisplayReponseInSeconds(respByTransaction));
            chart.ChartTitle.Text = "Response Time by Transaction";
            #endregion

            #region Response By Thread Sheet
            //Response by thread
            sheet = workbook.Worksheets.Add(After: workbook.Sheets[workbook.Sheets.Count]);
            sheet.Name = "Response by Threads";
            chartRange = ArrayToSheet(sheet, respByThreads);
            chart = CreateChart(sheet, chartRange, true, DisplayReponseInSeconds(respByThreads));
            chart.ChartTitle.Text = "Response Time by Thread Names";
            #endregion

            workbook.Worksheets[1].Activate();
            Console.WriteLine("Done.");

            //Save as .xlsx
            workbook.SaveAs(outputPath);
            workbook.Close();
            excel.Quit();

            Console.WriteLine("Excel saved: {0}", outputPath);
        }

        private Excel.Range ArrayToSheet(Excel.Worksheet sheet, Object[,] arr)
        {
            Excel.Range c1 = sheet.Cells[1, 1];
            Excel.Range c2 = sheet.Cells[arr.GetLength(0), arr.GetLength(1)];
            Excel.Range range = sheet.Range[c1, c2];
            range.Value = arr;

            //Formats time cells
            c1 = sheet.Cells[2, 1];
            c2 = sheet.Cells[arr.GetLength(0), 1];
            //sheet.Range[c1, c2].NumberFormat = "[$-F400]h:mm AM/PM";
            sheet.Range[c1, c2].NumberFormat = "hh:mm";
            
            return range;
        }

        private Excel.Chart CreateChart(Excel.Worksheet sheet, Excel.Range chartRange, bool addUserOverlay = false, bool displaySeconds = false)
        {
            if (chartRange.Columns.Count > 255)
            {
                String name = sheet.Name + " Chart";
                chartRange = chartRange.Columns["A:" + GetExcelColumnName(255)];
                sheet = sheet.Parent.Worksheets.Add(After: sheet.Parent.Sheets[sheet.Parent.Sheets.Count]);
                sheet.Name = name;
            }

            //Creates new line chart
            Excel.ChartObjects xlCharts = (Excel.ChartObjects)sheet.ChartObjects(Type.Missing);
            Excel.ChartObject myChart = xlCharts.Add(100, 10, 800, 400);
            Excel.Chart chartPage = myChart.Chart;

            chartPage.SetSourceData(chartRange, Excel.XlRowCol.xlColumns);
            chartPage.ChartType = Excel.XlChartType.xlLine;

            chartPage.ChartStyle = 227;
            chartPage.SetElement(ChartElement.msoElementPrimaryValueAxisTitleRotated);
            chartPage.SetElement(ChartElement.msoElementLegendTop);
            chartPage.SetElement(ChartElement.msoElementChartTitleAboveChart);

            //Changes scale of graph if average response time is larger than the threshold
            if (displaySeconds)
            {
                //Scales graph for seconds rather than milliseconds
                chartPage.Axes(Excel.XlAxisType.xlValue).AxisTitle.Text = "Response Time (Seconds)";
                chartPage.Axes(Excel.XlAxisType.xlValue).DisplayUnit = Excel.XlDisplayUnit.xlThousands;
                chartPage.Axes(Excel.XlAxisType.xlValue).HasDisplayUnitLabel = false;
            }
            else
            {
                chartPage.Axes(Excel.XlAxisType.xlValue).AxisTitle.Text = "Response Time (Milliseconds)";
            }

            if (addUserOverlay)
            {
                //Adds secondary axis for user overlay
                AddSecondaryOverlay(chartPage, userCol,"Numbers of users", "# of users");
            }

            return chartPage;
        }

        private void AddSecondaryOverlay(Excel.Chart chartPage, String col, String seriesName, String axisLabel, int colorVal = 16641995)
        {
            //Add number of users on second axis
            Excel.Series series = chartPage.SeriesCollection().NewSeries();
            series.Name = seriesName;
            series.Values = "='Total Results'!$" + col + "$2:$" + col + "$" + (maxPeriod + 1);
            series.ChartType = Excel.XlChartType.xlArea;
            series.AxisGroup = Excel.XlAxisGroup.xlSecondary;
            series.Format.Fill.ForeColor.RGB = colorVal;
            series.Format.Fill.Transparency = (float)0.4;
            chartPage.SetElement(ChartElement.msoElementSecondaryValueAxisTitleRotated);
            chartPage.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlSecondary).AxisTitle.Text = axisLabel;
        }

        /*private int RGB(int r, int g, int b)
        {
            return r + (g * 255) + (b * 255 * 255);
        }*/

        private string GetExcelColumnName(int columnNumber)
        {
            int div = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (div > 0)
            {
                modulo = (div - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                div = (int)((div - modulo) / 26);
            }

            return columnName;
        }

        private string FindColumnName(Excel.Range range, string searchVal)
        {
            string colName = "";

            foreach (Excel.Range cell in range)
            {
                if (!(cell.Value is null))
                {
                    if (cell.Value.ToString() == searchVal)
                    {   
                    colName = GetExcelColumnName(cell.Column);
                    }
                }
            }
            if (colName == "")
            {
                throw new Exception("\""+ searchVal + "\" column not found");
            }

            return colName;
        }

        private bool DisplayReponseInSeconds(Object[,] arr)
        {
            return GetAverageResponseTime(arr) > msThreshold;
        }

        private double GetAverageResponseTime(Excel.Range range)
        {
            //Finds average range of response times, removes first row and first column from the range to average
            Excel.Range avgRange;

            if (range.Areas.Count == 1)
            {
                avgRange = range.Columns["B:" + GetExcelColumnName(range.Columns.Count)];
                avgRange = avgRange.Rows["2:" + avgRange.Rows.Count];
            }
            else
            {
                avgRange = range.Areas[2].Rows["2:" + range.Rows.Count];
            }

            return GetAverage(avgRange);
        }

        private double GetAverageResponseTime(Object[,] arr)
        {
            return GetAverage(arr, 1, 1);
        }

        private double GetAverage(Excel.Range range)
        {
            int count = 0;
            double total = 0, val;

            foreach (Excel.Range cell in range.Cells)
            {
                val = cell.Value;
                if (val > 0)
                {
                    total += val;
                    count++;
                }
            }

            if (count > 0)
            {
                return total / count;
            }
            else
            {
                return 0;
            }
        }

        private double GetAverage(Object[,] arr, int startCol = 0, int startRow = 0)
        {
            int count = 0;
            double total = 0;

            for (int i = startRow; i < arr.GetLength(0); i++)
            {
                for (int j = startCol; j < arr.GetLength(1); j++)
                {
                    if (double.TryParse(arr[i, j].ToString(), out double val))
                    {
                        if (val > 0)
                        {
                            total += val;
                            count++;
                        }
                    }
                }
            }

            if (count > 0)
            {
                return total / count;
            }
            else
            {
                return 0;
            }
        }

        private int ColumnCount(Excel.Range range)
        {
            int count = 0;

            if (range.Areas.Count == 1)
            {
                count = range.Columns.Count;
            }
            else
            {
                foreach(Excel.Range area in range.Areas)
                {
                    count += area.Columns.Count;
                }
            }

            return count;
        }
    }
}