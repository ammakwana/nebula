﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using System.Text.RegularExpressions;

namespace Nebula_Excel
{
    class AccessDB
    {
        string connStr;
        List<string> transactions = new List<string>();
        List<string> threads = new List<string>();
        List<Time> times = new List<Time>();
        int startTime, endTime, timePeriod, maxPeriod;
        Object[,] respTotal, respByTransaction, respByThread;
        Object missingVal = "";

        public AccessDB(string fileName, int timePeriod = 60)
        {
            if (File.Exists(fileName))
            {
                //conn = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName);
                connStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName;
                this.timePeriod = timePeriod;
            }
            else
            {
                throw new Exception("File does not exist");
            }
        }

        public void AddTransactionNames()
        {
            string getTransactions = "select [Event_Map].[Event Name] from [Event_Map] where [Event_Map].[Event Type] = 'Transaction'";

            //Regex to match everything before the first underscore
            Regex threadRegex = new Regex(".+?(?=_|$)");

            try
            {
                using (OleDbConnection conn = new OleDbConnection(connStr))
                {
                    conn.Open();
                    using (OleDbCommand cmd = new OleDbCommand(getTransactions, conn))
                    {
                        using (OleDbDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                String str = reader.GetString(0);
                                transactions.Add(str);
                                Match m = threadRegex.Match(str);
                                if (m.Success)
                                {
                                    String threadName = m.Groups[0].Value;
                                    //Adds thread anme to list if it does not already exist in the list
                                    if (!threads.Contains(threadName)) threads.Add(threadName);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("OLEB: {0}", ex.Message);
            }
        }

        public string GetEvent(OleDbConnection conn, int start, int end)
        {
            string command = "select sum([Event_meter].[Acount]) as vcount, min([Event_meter].[Value] - [Event_meter].[think Time]) as vmin, sum(acount*(value-[think time]))/sum(acount) as vavg, max([Event_meter].[Value] - [Event_meter].[think Time]) as vmax from Result, Event_map, Event_meter where [Event_meter].[Event ID] = [Event_map].[Event ID] and ([Result].[Start Time]+[Event_meter].[End Time]) >= " + start + " and ([Result].[Start Time]+[Event_meter].[End Time]) < " + end + " AND Status1 = 1";
            string row = "NaN";

            try
            {
                using (OleDbCommand cmd = new OleDbCommand(command, conn))
                {
                    using (OleDbDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            if (!reader.IsDBNull(0))
                            {
                                row = string.Concat("transaction", ",", reader["vcount"], ",", reader["vmin"], ",", reader["vavg"], ",", reader["vmax"]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("OLEB: {0}", ex.Message);
            }

            return row;
        }

        public void Exec()
        {
            FindTimes();
            AddTransactionNames();
            QueryTotalResults();
            QueryResponseByTransaction();
            QueryResponseByThread();
        }
        
        public class Time
        {
            public string name;
            public int start;
            public int end;

            public Time(string name, int start, int end)
            {
                this.name = name;
                this.start = start;
                this.end = end;
            }
        }

        private void FindTimes()
        {
            String timeQuery = "SELECT MIN([Start Time]), MAX([Result End Time]) FROM Result";

            try
            {
                using (OleDbConnection conn = new OleDbConnection(connStr))
                {
                    conn.Open();
                    using (OleDbCommand cmd = new OleDbCommand(timeQuery, conn))
                    {
                        using (OleDbDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                startTime = reader.GetInt32(0);
                                endTime = reader.GetInt32(1);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("OLEB: {0}", ex.Message);
            }

            //for (int i = startTime; i < endTime; i += timePeriod)
            //{
            //    startTimeStr = UnixTimeStampToString(i);
            //    times.Add(new Time(startTimeStr, i, Math.Min(i + timePeriod, endTime)));
            //}

            maxPeriod = (int) Math.Ceiling((double)(endTime - startTime) / timePeriod);

        }

        private string UnixTimeStampToString(int i)
        {
            DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            return Epoch.AddSeconds(i).ToShortTimeString();
        }

        private Object[,] InitialiseObjectArray(String[] headerRow, Object missingVal)
        {
            Object[,] arr = new Object[maxPeriod + 1, headerRow.Length];

            //Add header row to array
            for (int j = 0; j < headerRow.Length; j++)
            {
                arr[0, j] = headerRow[j];
            }

            //Add time strings to time column
            for (int i = 0; i < maxPeriod; i++)
            {
                arr[i + 1, 0] = UnixTimeStampToString((i * timePeriod) + startTime);
            }

            //Add zero to all other values
            for (int i = 1; i < arr.GetLength(0); i++)
            {
                for (int j = 1; j < arr.GetLength(1); j++)
                {
                    arr[i, j] = missingVal;
                }
            }

            return arr;
        }

        private void QueryTotalResults()
        {
            string[] headerRow = new String[] {"Time", "TPS", "Response Time", "Threads", "Latency", "Success", "Error", "Error Rate (%)"};
            respTotal = InitialiseObjectArray(headerRow, 0);

            //Adds missing value for response time column
            for (int i = 1; i < respTotal.GetLength(0); i++)
            {
                respTotal[i, 2] = missingVal;
            }

            String respTimeQuery = String.Format("select TimeVals.timeCol, Success_Meter.vCount, Success_Meter.vMin, Success_Meter.vAvg, Success_Meter.vMax, Error_Meter.vCount as errorCount from ( ( select distinct round([End Time]/{0},0) as timeCol from Event_meter ) TimeVals left join ( select round([End Time]/{0},0) as timeCol, sum(Acount) as vCount, min(Value - [think Time]) as vMin, sum([acount]*(Value - [think time]))/sum(acount) as vAvg, max(Value- [think Time]) as vMax from Event_meter inner join Event_map on Event_meter.[Event ID] = Event_map.[Event ID] where Status1 = 1 and Event_map.[Event Type] = 'Transaction' group by round([End Time]/{0},0) ) Success_Meter on TimeVals.timeCol = Success_Meter.timeCol ) left join ( select round([End Time]/{0},0) as timeCol, sum(Acount) as vCount from Event_meter inner join Event_map on Event_meter.[Event ID] = Event_map.[Event ID] where Status1 = 0 and Event_map.[Event Type] = 'Transaction' group by round([End Time]/{0},0) ) Error_Meter on TimeVals.timeCol = Error_Meter.timeCol", timePeriod);
            String vUserQuery = String.Format("SELECT X.timeCol as timeCol, sum(Y.delta) as vUsers from ( SELECT distinct round([End Time]/{0},0) AS timeCol FROM vuserevent_meter WHERE   [vuser STATUS id] = 2 ) X left join ( SELECT round([End Time]/{0},0) AS timeCol, SUM([INOUT flag]) AS delta FROM vuserevent_meter WHERE [vuser STATUS id] = 2 GROUP BY round([End Time]/{0},0) ) Y on X.timeCol >= Y.timeCol group by X.timeCol", timePeriod);

            try
            {
                using (OleDbConnection conn = new OleDbConnection(connStr))
                {
                    conn.Open();

                    //Response time query execution
                    using (OleDbCommand cmd = new OleDbCommand(respTimeQuery, conn))
                    {
                        Console.Write("Starting 'Total Response Times' Query...");

                        using (OleDbDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //The reader wouldn't read this as an integer, so it is now Object > String > Integer
                                ReadQueryValue(reader, "timeCol", out int period);
                                ReadQueryValue(reader, "vCount", out int vCount);
                                ReadQueryValue(reader, "vAvg", out double vAvg);
                                ReadQueryValue(reader, "errorCount", out int errorCount);

                                if (period < maxPeriod)
                                {
                                    //Assign values to Object array
                                    respTotal[period + 1, 1] = (double)vCount / (double)timePeriod; //TPS
                                    respTotal[period + 1, 2] = vAvg * 1000; //Average Response Time
                                    respTotal[period + 1, 5] = vCount; //Success Count
                                    respTotal[period + 1, 6] = errorCount; //Error Count
                                    if ((errorCount + vCount) == 0)
                                    {
                                        respTotal[period + 1, 7] = 0;
                                    }
                                    else
                                    {
                                        respTotal[period + 1, 7] = ((double)errorCount / ((double)errorCount + (double)vCount)); //Error Rate (%)
                                    }
                                }
                            }
                        }

                        Console.WriteLine("Done.");
                    }

                    //VUser query execution
                    using (OleDbCommand cmd = new OleDbCommand(vUserQuery, conn))
                    {
                        Console.Write("Starting vUser Query...");

                        using (OleDbDataReader reader = cmd.ExecuteReader())
                        {
                            int lastPeriod = 0, lastVUsers = 0;

                            while (reader.Read())
                            {
                                ReadQueryValue(reader, "timeCol", out int period);
                                ReadQueryValue(reader, "vUsers", out int vUsers);
                                for (int i = lastPeriod; i < period; i++)
                                {
                                    respTotal[i + 1, 3] = lastVUsers; //Threads (# of Users)
                                }
                                lastPeriod = period;
                                lastVUsers = vUsers;
                            }
                        }

                        Console.WriteLine("Done.");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("OLEB: {0}", ex.Message);
            }
        }

        private void QueryResponseByTransaction()
        {
            string[] headerRow = new String[transactions.Count + 1];
            headerRow[0] = "Time";
            Array.Copy(transactions.ToArray(), 0, headerRow, 1, transactions.Count);

            respByTransaction = InitialiseObjectArray(headerRow, missingVal);
            
            try
            {
                Console.Write("Starting 'Response By Transaction' Query...");

                using (OleDbConnection conn = new OleDbConnection(connStr))
                {
                    conn.Open();

                    //Response time by transaction query execution
                    for (int i = 1; i < headerRow.Length; i++)
                    {
                        String respTimeQuery = String.Format("select round([End Time]/{0},0) as timeCol, sum(Acount) as vCount, min(Value - [think Time]) as vMin, sum([acount]*(Value - [think time]))/sum(acount) as vAvg, max(Value- [think Time]) as vMax from Event_meter inner join Event_map on Event_meter.[Event ID] = Event_map.[Event ID] where Status1 = 1 and Event_map.[Event Type] = 'Transaction' and Event_map.[Event Name] = '{1}' group by round([End Time]/{0},0)", timePeriod, headerRow[i]);

                        using (OleDbCommand cmd = new OleDbCommand(respTimeQuery, conn))
                        {
                            

                            using (OleDbDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    ReadQueryValue(reader, "timeCol", out int period);
                                    ReadQueryValue(reader, "vAvg", out double vAvg);

                                    if (period < maxPeriod)
                                    {
                                        //Assign average response time to Object array
                                        respByTransaction[period + 1, i] = vAvg * 1000;
                                    }
                                }
                            }
                        }
                    }
                }

                Console.WriteLine("Done.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("OLEB: {0}", ex.Message);
            }
        }

        private void QueryResponseByThread()
        {
            //Finds thread names
            Regex threadName = new Regex(".+?(?=_)");

            string[] headerRow = new String[threads.Count + 1];
            headerRow[0] = "Time";
            Array.Copy(threads.ToArray(), 0, headerRow, 1, threads.Count);

            respByThread = InitialiseObjectArray(headerRow, missingVal);

            try
            {
                Console.Write("Starting 'Response By Thread' Query...");

                using (OleDbConnection conn = new OleDbConnection(connStr))
                {
                    conn.Open();

                    //Response time by transaction query execution
                    for (int i = 1; i < headerRow.Length; i++)
                    {
                        String respTimeQuery = String.Format("select round([End Time]/{0},0) as timeCol, sum(Acount) as vCount, min(Value - [think Time]) as vMin, sum([acount]*(Value - [think time]))/sum(acount) as vAvg, max(Value- [think Time]) as vMax from Event_meter inner join Event_map on Event_meter.[Event ID] = Event_map.[Event ID] where Status1 = 1 and Event_map.[Event Type] = 'Transaction' and Event_map.[Event Name] LIKE '{1}_%' group by round([End Time]/{0},0)", timePeriod, headerRow[i]);
                        
                        using (OleDbCommand cmd = new OleDbCommand(respTimeQuery, conn))
                        {
                            using (OleDbDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    ReadQueryValue(reader, "timeCol", out int period);
                                    ReadQueryValue(reader, "vAvg", out double vAvg);

                                    if (period < maxPeriod)
                                    {
                                        //Assign average response time to Object array
                                        respByThread[period + 1, i] = vAvg * 1000;
                                    }
                                }
                            }
                        }
                    }
                }

                Console.WriteLine("Done.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("OLEB: {0}", ex.Message);
            }
        }

        private void ReadQueryValue(OleDbDataReader reader, String colName, out int val)
        {
            if (reader.IsDBNull(reader.GetOrdinal(colName)))
            {
                val = 0;
            }
            else
            {
                val = Int32.Parse(reader[colName].ToString());
            }
        }

        private void ReadQueryValue(OleDbDataReader reader, String colName, out double val)
        {
            if (reader.IsDBNull(reader.GetOrdinal(colName)))
            {
                val = 0;
            }
            else
            {
                val = Double.Parse(reader[colName].ToString());
            }
        }
        
        //GETTER METHODS

        public Object[,] GetTotalArray()
        {
            return respTotal;
        }

        public Object[,] GetResponseByTransaction()
        {
            return respByTransaction;
        }

        public Object[,] GetResponseByThread()
        {
            return respByThread;
        }
    }
}