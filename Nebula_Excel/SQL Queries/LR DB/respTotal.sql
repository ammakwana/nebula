select
	TimeVals.timeCol,
	Success_Meter.vCount,
	Success_Meter.vMin,
	Success_Meter.vAvg,
	Success_Meter.vMax,
	Error_Meter.vCount as errorCount
from
(
	(
		select
			distinct round([End Time]/60,0) as timeCol
		from
		Event_meter
	) TimeVals
	left join
	(
		select
		round([End Time]/60,0) as timeCol,
		sum(Acount) as vCount,
		min(Value - [think Time]) as vMin,
		sum([acount]*(Value - [think time]))/sum(acount) as vAvg,
		max(Value- [think Time]) as vMax
		from
		Event_meter
		inner join
		Event_map on Event_meter.[Event ID] = Event_map.[Event ID]
		where
		Status1 = 1
		and Event_map.[Event Type] = 'Transaction'
		group by
		round([End Time]/60,0)
	) Success_Meter on TimeVals.timeCol = Success_Meter.timeCol
)
left join 
(
	select
	round([End Time]/60,0) as timeCol,
	sum(Acount) as vCount
	from
	Event_meter
	inner join
	Event_map on Event_meter.[Event ID] = Event_map.[Event ID]
	where
	Status1 = 0
	and Event_map.[Event Type] = 'Transaction'
	group by
	round([End Time]/60,0)
) Error_Meter on TimeVals.timeCol = Error_Meter.timeCol