select
	round([End Time]/60,0) as timeCol,
	sum(Acount) as vCount,
	min(Value - [think Time]) as vMin,
	sum([acount]*(Value - [think time]))/sum(acount) as vAvg,
	max(Value- [think Time]) as vMax
from
	Event_meter
inner join
	Event_map on Event_meter.[Event ID] = Event_map.[Event ID]
where
	Status1 = 1
	and Event_map.[Event Type] = 'Transaction'
	and Event_map.[Event Name] = '{0}'
group by
	round([End Time]/60,0)