SELECT
	X.timeCol as timeCol,
	sum(Y.delta) as vUsers
from
(
	SELECT 
		distinct round([End Time]/60,0) AS timeCol
	FROM   
		vuserevent_meter
	WHERE  
		[vuser STATUS id] = 2
	) X
left join
(
	SELECT 
		round([End Time]/60,0) AS timeCol,
		SUM([INOUT flag]) AS delta		
	FROM   
		vuserevent_meter
	WHERE  
		[vuser STATUS id] = 2
	GROUP BY 
		round([End Time]/60,0)
) Y on X.timeCol >= Y.timeCol
group by
X.timeCol